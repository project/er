<?php // $Id$
/**
 * @file
 *
 * Theme functions for the Entity Relationships module.
 */

/**
 * Theme function for a single Entity Relationship Instance.
 */
function theme_er_instance($variables) {

  $er_instance = $variables['instance'];
  $side = isset($variables['side']) ? $variables['side'] : ER_FROM;

  switch ($side) {
    case ER_FROM:
      $from = $er_instance['entity_from_label'];
      $label = $er_instance['party_from_label'];
      $to = l($er_instance['entity_to_label'], $er_instance['entity_to_uri']['path']);

      $output = sprintf('%s %s %s', $from, $label, $to);
      break;
    case ER_TO:
      $from = l($er_instance['entity_from_label'], $er_instance['entity_from_uri']['path']);
      $label = $er_instance['party_to_label'];
      $to = $er_instance['entity_to_label'];

      $output = sprintf('%s %s %s', $to, $label, $from);

  }
  return '<div class="er-instance">' . $output . '</div>';
}

/**
 * Theme function for multiple Entity Relationship instances.
 */
function theme_er_instances($variables) {

  $instances = $variables['instances'];
  $side = isset($variables['side']) ? $variables['side'] : ER_FROM;
  $side_class = $side == ER_FROM ? 'er-from' : 'er-to';
  $output = '<div class="er-instances ' . $side_class . '">';
  foreach ($instances as $instance) {
    $output .= theme('er_instance', array('instance' => $instance, 'side' => $side));
  }
  $output .= '</div>';
  return $output;
}